package com.example;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import org.junit.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.FluxProcessor;
import reactor.core.publisher.Operators;

/**
 * @author: linfeng
 * @date: 2021/10/26 17:42
 */
public class MongoClientCreationTest {

    @Test
    public void testCreateMongoClientWithDefaultConfig(){
        MongoClient mongoClient = MongoClients.create();
//        mongoClient.listDatabaseNames().subscribe(new Operators.);
    }

    @Test
    public void testCreateMongoClientWithConnectionString(){
        String connectionString = "mongodb://localhost:27017/";
        MongoClient mongoClient = MongoClients.create(connectionString);
    }

    @Test
    public void testCreateMongoClientWithSettings(){
        MongoClient mongoClient = MongoClients.create(MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString("mongodb://localhost:27017"))
                .build());

    }
}
