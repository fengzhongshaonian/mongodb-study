package com.example;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.junit.Before;

public class BaseTest {

    private static final String DATABASE_NAME = "mongodb_study";

    private MongoClient client;
    private MongoDatabase database;

    @Before
    public void setUp(){
        // 连接本机的mongodb
        client = MongoClients.create("mongodb://localhost:27017");
        database = client.getDatabase(DATABASE_NAME);
    }

    protected MongoDatabase getDatabase(){
        return database;
    }
}
