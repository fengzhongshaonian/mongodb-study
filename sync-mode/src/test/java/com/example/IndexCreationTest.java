package com.example;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.Test;

/**
 * @author: linfeng
 * @date: 2021/10/21 11:10
 */
public class IndexCreationTest extends BaseTest{

    @Test
    public void testCreateIndex(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> students = database.getCollection("students");
        String name = students.createIndex(new Document("name", 1));
        System.out.println("成功创建索引，索引名称为：" + name); // 成功创建索引，索引名称为：name_1
    }

    @Test
    public void testCreateIndex2(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> students = database.getCollection("students");
        String name = students.createIndex(new Document("age", -1));
        System.out.println("成功创建索引，索引名称为：" + name); // 成功创建索引，索引名称为：age_-1
    }
}
