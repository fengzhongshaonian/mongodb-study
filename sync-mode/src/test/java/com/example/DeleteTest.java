package com.example;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;
import org.junit.Test;

/**
 * @author: linfeng
 * @date: 2021/10/21 10:41
 */
public class DeleteTest extends BaseTest{

    @Test
    public void testDeleteOne(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> students = database.getCollection("students");
        DeleteResult deleteResult = students.deleteOne(Filters.eq("name", "张三"));
        System.out.println("删除了" + deleteResult.getDeletedCount() + "条数据");
    }

    @Test
    public void testDeleteMany(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> students = database.getCollection("students");
        DeleteResult deleteResult = students.deleteMany(Filters.eq("name", "张三"));
        System.out.println("删除了" + deleteResult.getDeletedCount() + "条数据");
    }

    @Test
    public void testDeleteAllWithoutFilter(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> students = database.getCollection("students");
        DeleteResult deleteResult = students.deleteMany(new Document());
        System.out.println("删除了" + deleteResult.getDeletedCount() + "条数据");
    }
}
