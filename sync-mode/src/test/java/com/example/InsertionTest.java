package com.example;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class InsertionTest extends BaseTest{


    @Test
    public void testInsertOne(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> collection = database.getCollection("students");

        Document zhangsan = new Document();
        zhangsan.append("name", "张三");
        zhangsan.append("school", "某科技大学");
        zhangsan.append("age", Integer.valueOf(24));

        collection.insertOne(zhangsan);
        System.out.println(collection.countDocuments());
    }


    @Test
    public void testInsertMany(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> collection = database.getCollection("students");
        List<Document> documentList = new ArrayList<>(5);
        for (int i=0; i < 5; ++i){
            documentList.add(new Document().append("name", "user" + i).append("age", 21));
        }
        collection.insertMany(documentList);
    }
}
