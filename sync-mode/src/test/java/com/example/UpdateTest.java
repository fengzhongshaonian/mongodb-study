package com.example;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.junit.Test;

/**
 * @author: linfeng
 * @date: 2021/10/21 10:06
 */
public class UpdateTest extends BaseTest{

    @Test
    public void testUpdateOne(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> students = database.getCollection("students");
        UpdateResult updateResult = students.updateOne(Filters.eq("name", "张三"),
                Updates.set("school", "某科学的科技大学"));
        System.out.println("更新了" + updateResult.getModifiedCount() + "条数据");
    }

    @Test
    public void testUpdateMany(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> students = database.getCollection("students");
        UpdateResult updateResult = students.updateMany(Filters.eq("name", "张三"),
                Updates.set("school", "某科学的学学校"));
        System.out.println("更新了" + updateResult.getModifiedCount() +"条数据");
    }
}
