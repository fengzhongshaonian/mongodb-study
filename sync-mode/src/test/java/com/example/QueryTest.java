package com.example;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.junit.Test;

public class QueryTest extends BaseTest{

    @Test
    public void testQueryAll(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> collection = database.getCollection("students");
        FindIterable<Document> documents = collection.find();
        documents.forEach(document -> {
            System.out.println(document.toJson());
        });
    }

    @Test
    public void testQueryAllWithCursor(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> collection = database.getCollection("students");
        MongoCursor<Document> cursor = collection.find().cursor();

        try{
            while (cursor.hasNext()){
                Document document = cursor.next();
                System.out.println(document.toJson());
            }
        }finally {
            cursor.close();
        }

    }

    @Test
    public void testQueryAndGetFirstOne(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> collection = database.getCollection("students");
        Document first = collection.find().first();
        System.out.println(first.toJson());
    }


    @Test
    public void testQueryByOneCondition(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> collection = database.getCollection("students");
        FindIterable<Document> documents = collection.find(Filters.eq("name", "张三"));
        documents.forEach(document -> {
            System.out.println(document.toJson());
        });
    }


    @Test
    public void testQueryByManyCondition(){
        MongoDatabase database = getDatabase();
        MongoCollection<Document> collection = database.getCollection("students");
        FindIterable<Document> documents = collection.find(Filters.and(
                Filters.eq("name", "张三"),
                Filters.eq("_id", new ObjectId("616d6193824c4c3653a53ffa"))
        ));
        documents.forEach(document -> {
            System.out.println(document.toJson());
        });
    }
}
