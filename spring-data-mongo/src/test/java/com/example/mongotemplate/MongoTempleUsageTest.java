package com.example.mongotemplate;

import com.example.config.MongoConfig;
import com.example.entity.Student;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicUpdate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * 测试MongoTemplate
 *
 * @author linfeng
 * @date 2021/10/19
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MongoConfig.class)
public class MongoTempleUsageTest {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Test
    public void testInsert(){
        Student student = new Student();
        student.setName("玉树临风");
        student.setAge(24);
        mongoTemplate.insert(student);
    }

    @Test
    public void testQuery(){
        List<Student> students = mongoTemplate.find(Query.query(Criteria.where("name").is("玉树临风")), Student.class);
        students.forEach(System.out::println);
    }

    @Test
    public void testUpdate(){
        Query query = Query.query(Criteria.where("name").is("玉树临风"));
        List<Student> students = mongoTemplate.find(query, Student.class);
        UpdateResult updateResult = mongoTemplate.updateMulti(query,
                BasicUpdate.update("age", Integer.valueOf(25)), Student.class);
        System.out.println(updateResult.getModifiedCount());
    }

    @Test
    public void testDelete(){
        DeleteResult result = mongoTemplate.remove(Query.query(Criteria.where("name").is("玉树临风")), Student.class);
        System.out.println(result.getDeletedCount());
    }
}
