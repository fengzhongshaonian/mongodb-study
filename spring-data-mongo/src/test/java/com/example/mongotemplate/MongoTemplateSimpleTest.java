package com.example.mongotemplate;


import com.example.config.MongoConfig;
import com.example.entity.Student;
import com.mongodb.client.MongoClients;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

/**
 * 测试MongoTemplate
 *
 * @author linfeng
 * @date 2021/10/19
 */
public class MongoTemplateSimpleTest {

    private static final String DATABASE_NAME = "mongodb_study";

    @Test
    public void testQueryWithMongoTemplateWithoutSpringConfig(){
        // 使用MongoTemplate的第一种方法：直接创建MongoTemplate并指定MongoClient
        MongoOperations operations = new MongoTemplate(MongoClients.create("mongodb://localhost:27017/"), DATABASE_NAME);
        queryWith(operations);
    }

    private void queryWith(MongoOperations operations) {
        List<Student> students = operations.find(Query.query(Criteria.where("name").is("张三")), Student.class);
        students.forEach(System.out::println);
    }

    @Test
    public void testQueryWithMongoTemplateWithSpringConfig(){
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(MongoConfig.class);
        MongoTemplate bean = applicationContext.getBean(MongoTemplate.class);
        queryWith(bean);
    }
}
